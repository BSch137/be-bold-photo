const colors = require('windicss/colors')

export default {
  theme: {
    extend: {
      screens: {
        'sm': '640px',
        'md': '768px',
        'lg': '1024px',
        'xl': '1280px',
        '2xl': '1500px',
      },      
      fontFamily: {
        sans: ['Montserrat', 'sans-serif'],        
      },
      letterSpacing: {
        tagline: '-0.01em'
      },      
    },
  },
}
